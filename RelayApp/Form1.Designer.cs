﻿namespace RelayApp
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.eventLog1 = new System.Diagnostics.EventLog();
            this.LogView = new System.Windows.Forms.TextBox();
            this.Result_1 = new System.Windows.Forms.Button();
            this.GameState = new System.Windows.Forms.CheckBox();
            this.Result_2 = new System.Windows.Forms.Button();
            this.Result_3 = new System.Windows.Forms.Button();
            this.OSC_Log = new System.Windows.Forms.TextBox();
            this.Packet_Log = new System.Windows.Forms.TextBox();
            this.DebugSwitch = new System.Windows.Forms.CheckBox();
            this.D501 = new System.Windows.Forms.CheckBox();
            this.D502 = new System.Windows.Forms.CheckBox();
            this.D504 = new System.Windows.Forms.CheckBox();
            this.D505 = new System.Windows.Forms.CheckBox();
            this.D506 = new System.Windows.Forms.CheckBox();
            this.D507 = new System.Windows.Forms.CheckBox();
            this.D508 = new System.Windows.Forms.CheckBox();
            this.D509 = new System.Windows.Forms.CheckBox();
            this.D510 = new System.Windows.Forms.CheckBox();
            this.D512 = new System.Windows.Forms.CheckBox();
            this.D513 = new System.Windows.Forms.CheckBox();
            this.D514 = new System.Windows.Forms.CheckBox();
            this.D515 = new System.Windows.Forms.CheckBox();
            this.D516 = new System.Windows.Forms.CheckBox();
            this.D517 = new System.Windows.Forms.CheckBox();
            this.D518 = new System.Windows.Forms.CheckBox();
            this.D519 = new System.Windows.Forms.CheckBox();
            this.D520 = new System.Windows.Forms.CheckBox();
            this.CheckBox_reset = new System.Windows.Forms.Button();
            this.Result_6 = new System.Windows.Forms.Button();
            this.Result_5 = new System.Windows.Forms.Button();
            this.Result_4 = new System.Windows.Forms.Button();
            this.Result_9 = new System.Windows.Forms.Button();
            this.Result_8 = new System.Windows.Forms.Button();
            this.Result_7 = new System.Windows.Forms.Button();
            this.Result_12 = new System.Windows.Forms.Button();
            this.Result_11 = new System.Windows.Forms.Button();
            this.Result_10 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).BeginInit();
            this.SuspendLayout();
            // 
            // eventLog1
            // 
            this.eventLog1.SynchronizingObject = this;
            // 
            // LogView
            // 
            this.LogView.CausesValidation = false;
            this.LogView.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LogView.Location = new System.Drawing.Point(169, 12);
            this.LogView.Multiline = true;
            this.LogView.Name = "LogView";
            this.LogView.Size = new System.Drawing.Size(200, 687);
            this.LogView.TabIndex = 0;
            // 
            // Result_1
            // 
            this.Result_1.Font = new System.Drawing.Font("MS UI Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Result_1.Location = new System.Drawing.Point(12, 129);
            this.Result_1.Name = "Result_1";
            this.Result_1.Size = new System.Drawing.Size(150, 42);
            this.Result_1.TabIndex = 1;
            this.Result_1.Text = "パスボックスへ結果 1-1　送信";
            this.Result_1.UseVisualStyleBackColor = true;
            this.Result_1.Click += new System.EventHandler(this.Result_11_Click);
            // 
            // GameState
            // 
            this.GameState.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GameState.AutoCheck = false;
            this.GameState.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GameState.CausesValidation = false;
            this.GameState.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.GameState.Location = new System.Drawing.Point(12, 12);
            this.GameState.Name = "GameState";
            this.GameState.Size = new System.Drawing.Size(150, 111);
            this.GameState.TabIndex = 2;
            this.GameState.Text = "ゲーム実行中";
            this.GameState.UseVisualStyleBackColor = false;
            // 
            // Result_2
            // 
            this.Result_2.Font = new System.Drawing.Font("MS UI Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Result_2.Location = new System.Drawing.Point(12, 177);
            this.Result_2.Name = "Result_2";
            this.Result_2.Size = new System.Drawing.Size(150, 42);
            this.Result_2.TabIndex = 3;
            this.Result_2.Text = "パスボックスへ結果 1-2　送信";
            this.Result_2.UseVisualStyleBackColor = true;
            this.Result_2.Click += new System.EventHandler(this.Result_12_Click);
            // 
            // Result_3
            // 
            this.Result_3.Font = new System.Drawing.Font("MS UI Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Result_3.Location = new System.Drawing.Point(12, 225);
            this.Result_3.Name = "Result_3";
            this.Result_3.Size = new System.Drawing.Size(150, 42);
            this.Result_3.TabIndex = 4;
            this.Result_3.Text = "パスボックスへ結果 1-3　送信";
            this.Result_3.UseVisualStyleBackColor = true;
            this.Result_3.Click += new System.EventHandler(this.Result_13_Click);
            // 
            // OSC_Log
            // 
            this.OSC_Log.CausesValidation = false;
            this.OSC_Log.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.OSC_Log.Location = new System.Drawing.Point(375, 12);
            this.OSC_Log.Multiline = true;
            this.OSC_Log.Name = "OSC_Log";
            this.OSC_Log.Size = new System.Drawing.Size(200, 350);
            this.OSC_Log.TabIndex = 6;
            // 
            // Packet_Log
            // 
            this.Packet_Log.CausesValidation = false;
            this.Packet_Log.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Packet_Log.Location = new System.Drawing.Point(375, 372);
            this.Packet_Log.Multiline = true;
            this.Packet_Log.Name = "Packet_Log";
            this.Packet_Log.Size = new System.Drawing.Size(200, 80);
            this.Packet_Log.TabIndex = 7;
            // 
            // DebugSwitch
            // 
            this.DebugSwitch.AutoSize = true;
            this.DebugSwitch.Location = new System.Drawing.Point(581, 12);
            this.DebugSwitch.Name = "DebugSwitch";
            this.DebugSwitch.Size = new System.Drawing.Size(87, 16);
            this.DebugSwitch.TabIndex = 8;
            this.DebugSwitch.Text = "デバックモード";
            this.DebugSwitch.UseVisualStyleBackColor = true;
            // 
            // D501
            // 
            this.D501.AutoSize = true;
            this.D501.Location = new System.Drawing.Point(581, 34);
            this.D501.Name = "D501";
            this.D501.Size = new System.Drawing.Size(93, 16);
            this.D501.TabIndex = 9;
            this.D501.Text = "PLCライフビット";
            this.D501.UseVisualStyleBackColor = true;
            // 
            // D502
            // 
            this.D502.AutoSize = true;
            this.D502.Location = new System.Drawing.Point(581, 56);
            this.D502.Name = "D502";
            this.D502.Size = new System.Drawing.Size(96, 16);
            this.D502.TabIndex = 10;
            this.D502.Text = "小窓「開」可能";
            this.D502.UseVisualStyleBackColor = true;
            // 
            // D504
            // 
            this.D504.AutoSize = true;
            this.D504.Location = new System.Drawing.Point(581, 78);
            this.D504.Name = "D504";
            this.D504.Size = new System.Drawing.Size(130, 16);
            this.D504.TabIndex = 11;
            this.D504.Text = "PLCパスゲーム受付中";
            this.D504.UseVisualStyleBackColor = true;
            // 
            // D505
            // 
            this.D505.AutoSize = true;
            this.D505.Location = new System.Drawing.Point(581, 100);
            this.D505.Name = "D505";
            this.D505.Size = new System.Drawing.Size(73, 16);
            this.D505.TabIndex = 12;
            this.D505.Text = "パス1予約";
            this.D505.UseVisualStyleBackColor = true;
            // 
            // D506
            // 
            this.D506.AutoSize = true;
            this.D506.Location = new System.Drawing.Point(581, 122);
            this.D506.Name = "D506";
            this.D506.Size = new System.Drawing.Size(73, 16);
            this.D506.TabIndex = 13;
            this.D506.Text = "パス2予約";
            this.D506.UseVisualStyleBackColor = true;
            // 
            // D507
            // 
            this.D507.AutoSize = true;
            this.D507.Location = new System.Drawing.Point(581, 144);
            this.D507.Name = "D507";
            this.D507.Size = new System.Drawing.Size(73, 16);
            this.D507.TabIndex = 14;
            this.D507.Text = "パス3予約";
            this.D507.UseVisualStyleBackColor = true;
            // 
            // D508
            // 
            this.D508.AutoSize = true;
            this.D508.Location = new System.Drawing.Point(581, 166);
            this.D508.Name = "D508";
            this.D508.Size = new System.Drawing.Size(79, 16);
            this.D508.TabIndex = 15;
            this.D508.Text = "パス動作中";
            this.D508.UseVisualStyleBackColor = true;
            // 
            // D509
            // 
            this.D509.AutoSize = true;
            this.D509.Location = new System.Drawing.Point(581, 188);
            this.D509.Name = "D509";
            this.D509.Size = new System.Drawing.Size(91, 16);
            this.D509.TabIndex = 16;
            this.D509.Text = "パス排出完了";
            this.D509.UseVisualStyleBackColor = true;
            // 
            // D510
            // 
            this.D510.AutoSize = true;
            this.D510.Location = new System.Drawing.Point(581, 210);
            this.D510.Name = "D510";
            this.D510.Size = new System.Drawing.Size(113, 16);
            this.D510.TabIndex = 17;
            this.D510.Text = "製品取り出し要求";
            this.D510.UseVisualStyleBackColor = true;
            // 
            // D512
            // 
            this.D512.AutoSize = true;
            this.D512.Location = new System.Drawing.Point(581, 232);
            this.D512.Name = "D512";
            this.D512.Size = new System.Drawing.Size(130, 16);
            this.D512.TabIndex = 18;
            this.D512.Text = "PLCデモゲーム受付中";
            this.D512.UseVisualStyleBackColor = true;
            // 
            // D513
            // 
            this.D513.AutoSize = true;
            this.D513.Location = new System.Drawing.Point(581, 254);
            this.D513.Name = "D513";
            this.D513.Size = new System.Drawing.Size(146, 16);
            this.D513.TabIndex = 19;
            this.D513.Text = "ロボ仕事中デモ受付不可";
            this.D513.UseVisualStyleBackColor = true;
            // 
            // D514
            // 
            this.D514.AutoSize = true;
            this.D514.Location = new System.Drawing.Point(581, 276);
            this.D514.Name = "D514";
            this.D514.Size = new System.Drawing.Size(97, 16);
            this.D514.TabIndex = 20;
            this.D514.Text = "デモ1動作開始";
            this.D514.UseVisualStyleBackColor = true;
            // 
            // D515
            // 
            this.D515.AutoSize = true;
            this.D515.Location = new System.Drawing.Point(581, 298);
            this.D515.Name = "D515";
            this.D515.Size = new System.Drawing.Size(97, 16);
            this.D515.TabIndex = 21;
            this.D515.Text = "デモ2動作開始";
            this.D515.UseVisualStyleBackColor = true;
            // 
            // D516
            // 
            this.D516.AutoSize = true;
            this.D516.Location = new System.Drawing.Point(581, 320);
            this.D516.Name = "D516";
            this.D516.Size = new System.Drawing.Size(97, 16);
            this.D516.TabIndex = 22;
            this.D516.Text = "デモ3動作開始";
            this.D516.UseVisualStyleBackColor = true;
            // 
            // D517
            // 
            this.D517.AutoSize = true;
            this.D517.Location = new System.Drawing.Point(581, 342);
            this.D517.Name = "D517";
            this.D517.Size = new System.Drawing.Size(97, 16);
            this.D517.TabIndex = 23;
            this.D517.Text = "デモ4動作開始";
            this.D517.UseVisualStyleBackColor = true;
            // 
            // D518
            // 
            this.D518.AutoSize = true;
            this.D518.Location = new System.Drawing.Point(581, 365);
            this.D518.Name = "D518";
            this.D518.Size = new System.Drawing.Size(67, 16);
            this.D518.TabIndex = 24;
            this.D518.Text = "動作なし";
            this.D518.UseVisualStyleBackColor = true;
            // 
            // D519
            // 
            this.D519.AutoSize = true;
            this.D519.Location = new System.Drawing.Point(581, 388);
            this.D519.Name = "D519";
            this.D519.Size = new System.Drawing.Size(96, 16);
            this.D519.TabIndex = 25;
            this.D519.Text = "動作正常終了";
            this.D519.UseVisualStyleBackColor = true;
            // 
            // D520
            // 
            this.D520.AutoSize = true;
            this.D520.Location = new System.Drawing.Point(581, 411);
            this.D520.Name = "D520";
            this.D520.Size = new System.Drawing.Size(96, 16);
            this.D520.TabIndex = 26;
            this.D520.Text = "動作強制終了";
            this.D520.UseVisualStyleBackColor = true;
            // 
            // CheckBox_reset
            // 
            this.CheckBox_reset.Location = new System.Drawing.Point(581, 429);
            this.CheckBox_reset.Name = "CheckBox_reset";
            this.CheckBox_reset.Size = new System.Drawing.Size(75, 23);
            this.CheckBox_reset.TabIndex = 28;
            this.CheckBox_reset.Text = "リセット";
            this.CheckBox_reset.UseVisualStyleBackColor = true;
            this.CheckBox_reset.Click += new System.EventHandler(this.CheckBox_Reset_Click);
            // 
            // Result_6
            // 
            this.Result_6.Font = new System.Drawing.Font("MS UI Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Result_6.Location = new System.Drawing.Point(12, 369);
            this.Result_6.Name = "Result_6";
            this.Result_6.Size = new System.Drawing.Size(150, 42);
            this.Result_6.TabIndex = 31;
            this.Result_6.Text = "パスボックスへ結果 2-3　送信";
            this.Result_6.UseVisualStyleBackColor = true;
            this.Result_6.Click += new System.EventHandler(this.Result_23_Click);
            // 
            // Result_5
            // 
            this.Result_5.Font = new System.Drawing.Font("MS UI Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Result_5.Location = new System.Drawing.Point(12, 319);
            this.Result_5.Name = "Result_5";
            this.Result_5.Size = new System.Drawing.Size(150, 42);
            this.Result_5.TabIndex = 30;
            this.Result_5.Text = "パスボックスへ結果 2-2　送信";
            this.Result_5.UseVisualStyleBackColor = true;
            this.Result_5.Click += new System.EventHandler(this.Result_22_Click);
            // 
            // Result_4
            // 
            this.Result_4.Font = new System.Drawing.Font("MS UI Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Result_4.Location = new System.Drawing.Point(12, 273);
            this.Result_4.Name = "Result_4";
            this.Result_4.Size = new System.Drawing.Size(150, 42);
            this.Result_4.TabIndex = 29;
            this.Result_4.Text = "パスボックスへ結果 2-1　送信";
            this.Result_4.UseVisualStyleBackColor = true;
            this.Result_4.Click += new System.EventHandler(this.Result_21_Click);
            // 
            // Result_9
            // 
            this.Result_9.Font = new System.Drawing.Font("MS UI Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Result_9.Location = new System.Drawing.Point(12, 513);
            this.Result_9.Name = "Result_9";
            this.Result_9.Size = new System.Drawing.Size(150, 42);
            this.Result_9.TabIndex = 34;
            this.Result_9.Text = "パスボックスへ結果 3-3　送信";
            this.Result_9.UseVisualStyleBackColor = true;
            this.Result_9.Click += new System.EventHandler(this.Result_33_Click);
            // 
            // Result_8
            // 
            this.Result_8.Font = new System.Drawing.Font("MS UI Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Result_8.Location = new System.Drawing.Point(12, 465);
            this.Result_8.Name = "Result_8";
            this.Result_8.Size = new System.Drawing.Size(150, 42);
            this.Result_8.TabIndex = 33;
            this.Result_8.Text = "パスボックスへ結果 3-2　送信";
            this.Result_8.UseVisualStyleBackColor = true;
            this.Result_8.Click += new System.EventHandler(this.Result_32_Click);
            // 
            // Result_7
            // 
            this.Result_7.Font = new System.Drawing.Font("MS UI Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Result_7.Location = new System.Drawing.Point(12, 417);
            this.Result_7.Name = "Result_7";
            this.Result_7.Size = new System.Drawing.Size(150, 42);
            this.Result_7.TabIndex = 32;
            this.Result_7.Text = "パスボックスへ結果 3-1　送信";
            this.Result_7.UseVisualStyleBackColor = true;
            this.Result_7.Click += new System.EventHandler(this.Result_31_Click);
            // 
            // Result_12
            // 
            this.Result_12.Font = new System.Drawing.Font("MS UI Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Result_12.Location = new System.Drawing.Point(13, 657);
            this.Result_12.Name = "Result_12";
            this.Result_12.Size = new System.Drawing.Size(150, 42);
            this.Result_12.TabIndex = 37;
            this.Result_12.Text = "パスボックスへ結果 4-3　送信";
            this.Result_12.UseVisualStyleBackColor = true;
            this.Result_12.Click += new System.EventHandler(this.Result_43_Click);
            // 
            // Result_11
            // 
            this.Result_11.Font = new System.Drawing.Font("MS UI Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Result_11.Location = new System.Drawing.Point(12, 609);
            this.Result_11.Name = "Result_11";
            this.Result_11.Size = new System.Drawing.Size(150, 42);
            this.Result_11.TabIndex = 36;
            this.Result_11.Text = "パスボックスへ結果 4-2　送信";
            this.Result_11.UseVisualStyleBackColor = true;
            this.Result_11.Click += new System.EventHandler(this.Result_42_Click);
            // 
            // Result_10
            // 
            this.Result_10.Font = new System.Drawing.Font("MS UI Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Result_10.Location = new System.Drawing.Point(12, 561);
            this.Result_10.Name = "Result_10";
            this.Result_10.Size = new System.Drawing.Size(150, 42);
            this.Result_10.TabIndex = 35;
            this.Result_10.Text = "パスボックスへ結果 4-1　送信";
            this.Result_10.UseVisualStyleBackColor = true;
            this.Result_10.Click += new System.EventHandler(this.Result_41_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(374, 711);
            this.Controls.Add(this.Result_12);
            this.Controls.Add(this.Result_11);
            this.Controls.Add(this.Result_10);
            this.Controls.Add(this.Result_9);
            this.Controls.Add(this.Result_8);
            this.Controls.Add(this.Result_7);
            this.Controls.Add(this.Result_6);
            this.Controls.Add(this.Result_5);
            this.Controls.Add(this.Result_4);
            this.Controls.Add(this.CheckBox_reset);
            this.Controls.Add(this.D520);
            this.Controls.Add(this.D519);
            this.Controls.Add(this.D518);
            this.Controls.Add(this.D517);
            this.Controls.Add(this.D516);
            this.Controls.Add(this.D515);
            this.Controls.Add(this.D514);
            this.Controls.Add(this.D513);
            this.Controls.Add(this.D512);
            this.Controls.Add(this.D510);
            this.Controls.Add(this.D509);
            this.Controls.Add(this.D508);
            this.Controls.Add(this.D507);
            this.Controls.Add(this.D506);
            this.Controls.Add(this.D505);
            this.Controls.Add(this.D504);
            this.Controls.Add(this.D502);
            this.Controls.Add(this.D501);
            this.Controls.Add(this.DebugSwitch);
            this.Controls.Add(this.Packet_Log);
            this.Controls.Add(this.OSC_Log);
            this.Controls.Add(this.Result_3);
            this.Controls.Add(this.Result_2);
            this.Controls.Add(this.GameState);
            this.Controls.Add(this.Result_1);
            this.Controls.Add(this.LogView);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "通信中継アプリ";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Diagnostics.EventLog eventLog1;
        private System.Windows.Forms.TextBox LogView;
        private System.Windows.Forms.Button Result_1;
        private System.Windows.Forms.CheckBox GameState;
        private System.Windows.Forms.Button Result_3;
        private System.Windows.Forms.Button Result_2;
        private System.Windows.Forms.TextBox OSC_Log;
        private System.Windows.Forms.TextBox Packet_Log;
        private System.Windows.Forms.CheckBox DebugSwitch;
        private System.Windows.Forms.CheckBox D506;
        private System.Windows.Forms.CheckBox D505;
        private System.Windows.Forms.CheckBox D504;
        private System.Windows.Forms.CheckBox D502;
        private System.Windows.Forms.CheckBox D501;
        private System.Windows.Forms.CheckBox D512;
        private System.Windows.Forms.CheckBox D510;
        private System.Windows.Forms.CheckBox D509;
        private System.Windows.Forms.CheckBox D508;
        private System.Windows.Forms.CheckBox D507;
        private System.Windows.Forms.CheckBox D517;
        private System.Windows.Forms.CheckBox D516;
        private System.Windows.Forms.CheckBox D515;
        private System.Windows.Forms.CheckBox D514;
        private System.Windows.Forms.CheckBox D513;
        private System.Windows.Forms.CheckBox D520;
        private System.Windows.Forms.CheckBox D519;
        private System.Windows.Forms.CheckBox D518;
        private System.Windows.Forms.Button CheckBox_reset;
        private System.Windows.Forms.Button Result_6;
        private System.Windows.Forms.Button Result_5;
        private System.Windows.Forms.Button Result_4;
        private System.Windows.Forms.Button Result_9;
        private System.Windows.Forms.Button Result_8;
        private System.Windows.Forms.Button Result_7;
        private System.Windows.Forms.Button Result_12;
        private System.Windows.Forms.Button Result_11;
        private System.Windows.Forms.Button Result_10;
    }
}

