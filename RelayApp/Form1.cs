﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MITSUBISHI.Component;
using Rug.Osc;
using System.IO;
using System.Net;

namespace RelayApp
{
    enum DeviceID
    {
        D501 = 0,
        D502 = 1,
        D504 = 2,
        D505 = 3,
        D506 = 4,
        D507 = 5,
        D508 = 6,
        D509 = 7,
        D510 = 8,
        D512 = 9,
        D513 = 10,
        D514 = 11,
        D515 = 12,
        D516 = 13,
        D517 = 14,
        D518 = 15,
        D519 = 16,
        D520 = 17,
        D001 = 18,
        D002 = 19,
        D004 = 20,
        D005 = 21,
        D006 = 22,
    };

    //Windowsフォームアプリケーションクラス
    public partial class MainForm : Form
    {
        DotUtlType dotUtlType = new DotUtlType() { ActLogicalStationNumber = Properties.Settings.Default.StationNumber };
        string Label;//項目名
        int BitData;//値
        int DataBox;//データの格納先
        public static string ReceiveData = "";

        public MainForm()
        {
            InitializeComponent();
            dotUtlType.Open();
        }
        //アプリが起動したら
        private void Form1_Load(object sender, EventArgs e)
        {
            DebugSwitch.Checked = Properties.Settings.Default.DebugModeFlag;
            OSCDatasRefresh();
            StatusUpdate();
            PCLifeBit();

            OSC.Receiver();
            //OSC_Log.AppendText(ReceiveData);
            ListView listView = new ListView();
        }
        //アプリが閉じる前
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //終了時PCライフビットを0にする
            OSCDatasRefresh();
            OSC.OscClosed();
        }
        //アプリが閉じた後
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
        //PLCへのライフビットの更新
        public async void PCLifeBit()
        {
            System.TimeSpan IntervalTime = Properties.Settings.Default.PCLifeBitUpdateInterval;//1000
            bool Flag = false;

            for (; ; )
            {
                if (Flag == false)
                {
                    Label = "PCライフビット";
                    BitData = 0;
                    DataBox = dotUtlType.SetDevice(ref Label, BitData);
                    Flag = true;
                }
                else if (Flag == true)
                {
                    Label = "PCライフビット";
                    BitData = 1;
                    DataBox = dotUtlType.SetDevice(ref Label, BitData);
                    Flag = false;
                }
                //間隔待ち
                await Task.Delay((int)IntervalTime.TotalMilliseconds);
            }
        }
        //起動にパスボックスへ投げるデータを一度0にしてリセットしておく
        public void OSCDatasRefresh()
        {
            //終了時PCライフビットを0にする
            //PCライフビット
            {
                Label = "PCライフビット";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            //ゲーム中
            {
                Label = "ゲーム中";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
                GameState.Checked = false;
            }
            //結果1-1
            {
                Label = "結果1の1";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            //結果1-2
            {
                Label = "結果1の2";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            //結果1-3
            {
                Label = "結果1の3";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            //結果2-1
            {
                Label = "結果2の1";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            //結果2-2
            {
                Label = "結果2の2";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            //結果2-3
            {
                Label = "結果2の3";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            //結果3-1
            {
                Label = "結果3の1";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            //結果3-2
            {
                Label = "結果3の2";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            //結果3-3
            {
                Label = "結果3の3";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            //結果4-1
            {
                Label = "結果4の1";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            //結果4-2
            {
                Label = "結果4の2";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            //結果4-3
            {
                Label = "結果4の3";
                BitData = 0;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
        }
        //GameState更新
        public async void OSCGameStateCheck(string StateNum)
        {
            switch (StateNum)
            {
                case "/gamestate, 0":
                    //ゲーム終了
                    {
                        Label = "ゲーム中";
                        BitData = 0;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        GameState.Checked = false;
                    }
                    break;
                case "/gamestate, 1":
                    //ゲーム中
                    {
                        Label = "ゲーム中";
                        BitData = 1;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        GameState.Checked = true;
                    }
                    break;
                case "/result1, 0":
                    //結果1-1~1-3
                    {
                        Label = "ゲーム中";
                        BitData = 0;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        GameState.Checked = false;
                        Label = "結果1の1";
                        BitData = 0;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        Label = "結果1の2";
                        BitData = 0;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        Label = "結果1の3";
                        BitData = 0;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                    }
                    break;

                    //1
                case "/result1, 1":
                    //結果1-1
                    {
                        Label = "結果1の1";
                        BitData = 1;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        await Task.Delay(3000);
                        BitRefresh_0("結果1の1");
                    }
                    break;
                case "/result1, 2":
                    //結果1-2
                    {
                        Label = "結果1の2";
                        BitData = 1;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        await Task.Delay(3000);
                        BitRefresh_0("結果1の2");
                    }
                    break;
                case "/result1, 3":
                    //結果1-3
                    {
                        Label = "結果1の3";
                        BitData = 1;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        await Task.Delay(3000);
                        BitRefresh_0("結果1の3");
                    }
                    break;

                    //2
                case "/result2, 1":
                    //結果2-1
                    {
                        Label = "結果2の1";
                        BitData = 1;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        await Task.Delay(3000);
                        BitRefresh_0("結果2の1");
                    }
                    break;
                case "/result2, 2":
                    //結果2-2
                    {
                        Label = "結果2の2";
                        BitData = 1;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        await Task.Delay(3000);
                        BitRefresh_0("結果2の2");
                    }
                    break;
                case "/result2, 3":
                    //結果2-3
                    {
                        Label = "結果2の3";
                        BitData = 1;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        await Task.Delay(3000);
                        BitRefresh_0("結果2の3");
                    }
                    break;

                    //3
                case "/result3, 1":
                    //結果3-1
                    {
                        Label = "結果3の1";
                        BitData = 1;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        await Task.Delay(3000);
                        BitRefresh_0("結果3の1");
                    }
                    break;
                case "/result3, 2":
                    //結果3-2
                    {
                        Label = "結果3の2";
                        BitData = 1;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        await Task.Delay(3000);
                        BitRefresh_0("結果3の2");
                    }
                    break;
                case "/result3, 3":
                    //結果3-3
                    {
                        Label = "結果3の3";
                        BitData = 1;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        await Task.Delay(3000);
                        BitRefresh_0("結果3の3");
                    }
                    break;
                    
                    //4
                case "/result4, 1":
                    //結果4-1
                    {
                        Label = "結果4の1";
                        BitData = 1;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        await Task.Delay(3000);
                        BitRefresh_0("結果4の1");
                    }
                    break;
                case "/result4, 2":
                    //結果4-2
                    {
                        Label = "結果4の2";
                        BitData = 1;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        await Task.Delay(3000);
                        BitRefresh_0("結果4の2");
                    }
                    break;
                case "/result4, 3":
                    //結果4-3
                    {
                        Label = "結果4の3";
                        BitData = 1;
                        DataBox = dotUtlType.SetDevice(ref Label, BitData);
                        await Task.Delay(3000);
                        BitRefresh_0("結果4の3");
                    }
                    break;
            }
        }
        //ライフビット用の更新
        public async void StatusUpdate()
        {
            System.TimeSpan IntervalTime = Properties.Settings.Default.StatusUpdateInterval;//1000
            int[] Old_D = new int[21];
            int[] Old_D_Comparison = new int[21];
            int[] BitDataToLog = new int[21];

            for (; ; )
            {
                //PLCから受け取るデータ表示
                {
                    //項目説明
                    {
                        LogView.Text = "//パスボックスRead" + "\r\n";
                        OSC_Log.Text = "//OSC送信" + "\r\n";
                    }
                    //D10501,D10502,D10504～D10510
                    {
                        //D10501,0
                        {
                            Label = "PLCライフビット";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("PLCライフビット:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                OSC.Sender(new OscMessage("/CLD501", BitData));
                                OSC_Log.AppendText(Label + ":" + BitData + "\r\n");
                            }
                            else
                            {
                                if (D501.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD501", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD501", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10502,1
                        {
                            Label = "小窓「開」可能";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("小窓「開」可能:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D502] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D502] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D502] = Old_D[(int)DeviceID.D502];
                                    OSC.Sender(new OscMessage("/CLD502", BitData));
                                }
                                OSC_Log.AppendText("小窓「開」可能:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D502.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD502", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD502", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10504,2
                        {
                            Label = "PLCパスゲーム受付中";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("PLCパスゲーム受付中:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D504] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D504] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D504] = Old_D[(int)DeviceID.D504];
                                    OSC.Sender(new OscMessage("/CLD504", BitData));
                                }
                                OSC_Log.AppendText("PLCパスゲーム受付中:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D504.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD504", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD504", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10505,3
                        {
                            Label = "パス1予約";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("パス1予約:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D505] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D505] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D505] = Old_D[(int)DeviceID.D505];
                                    OSC.Sender(new OscMessage("/CLD505", BitData));
                                }
                                OSC_Log.AppendText("パス1予約:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D505.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD505", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD505", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10506,4
                        {
                            Label = "パス2予約";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("パス2予約:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D506] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D506] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D506] = Old_D[(int)DeviceID.D506];
                                    OSC.Sender(new OscMessage("/CLD506", BitData));
                                }
                                OSC_Log.AppendText("パス2予約:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D506.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD506", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD506", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10507,5
                        {
                            Label = "パス3予約";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("パス3予約:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D507] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D507] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D507] = Old_D[(int)DeviceID.D507];
                                    OSC.Sender(new OscMessage("/CLD507", BitData));
                                }
                                OSC_Log.AppendText("パス3予約:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D507.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD507", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD507", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10508,6
                        {
                            Label = "パス動作中";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("パス動作中:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D508] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D508] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D508] = Old_D[(int)DeviceID.D508];
                                    OSC.Sender(new OscMessage("/CLD508", BitData));
                                }
                                OSC_Log.AppendText("パス動作中:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D508.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD508", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD508", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10509,7
                        {
                            Label = "パス排出完了";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("パス排出完了:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D509] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D509] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D509] = Old_D[(int)DeviceID.D509];
                                    OSC.Sender(new OscMessage("/CLD509", BitData));
                                }
                                OSC_Log.AppendText("パス排出完了:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D509.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD509", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD509", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10510,8
                        {
                            Label = "製品取り出し要求";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("製品取り出し要求:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D510] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D510] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D510] = Old_D[(int)DeviceID.D510];
                                    OSC.Sender(new OscMessage("/CLD510", BitData));
                                }
                                OSC_Log.AppendText("製品取り出し要求:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D510.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD510", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD510", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                    }
                    //D10512～D10517
                    {
                        //D10512,9
                        {
                            Label = "PLCデモゲーム受付中";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("PLCデモゲーム受付中:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D512] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D512] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D512] = Old_D[(int)DeviceID.D512];
                                    OSC.Sender(new OscMessage("/CLD512", BitData));
                                }
                                OSC_Log.AppendText("PLCデモゲーム受付中:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D512.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD512", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD512", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10513,10
                        {
                            Label = "ロボ仕事中デモ受付不可";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("ロボ仕事中デモ受付不可:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D513] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D513] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D513] = Old_D[(int)DeviceID.D513];
                                    OSC.Sender(new OscMessage("/CLD513", BitData));
                                }
                                OSC_Log.AppendText("ロボ仕事中デモ受付不可:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D513.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD513", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD513", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10514,11
                        {
                            Label = "デモ1動作開始";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("デモ1動作開始:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D514] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D514] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D514] = Old_D[(int)DeviceID.D514];
                                    OSC.Sender(new OscMessage("/CLD514", BitData));
                                }
                                OSC_Log.AppendText("デモ1動作開始:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D514.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD514", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD514", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10515,12
                        {
                            Label = "デモ2動作開始";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("デモ2動作開始:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D515] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D515] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D515] = Old_D[(int)DeviceID.D515];
                                    OSC.Sender(new OscMessage("/CLD515", BitData));
                                }
                                OSC_Log.AppendText("デモ2動作開始:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D515.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD515", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD515", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10516,13
                        {
                            Label = "デモ3動作開始";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("デモ3動作開始:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D516] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D516] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D516] = Old_D[(int)DeviceID.D516];
                                    OSC.Sender(new OscMessage("/CLD516", BitData));
                                }
                                OSC_Log.AppendText("デモ3動作開始:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D516.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD516", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD516", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10517,14
                        {
                            Label = "デモ4動作開始";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("デモ4動作開始:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D517] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D517] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D517] = Old_D[(int)DeviceID.D517];
                                    OSC.Sender(new OscMessage("/CLD517", BitData));
                                }
                                OSC_Log.AppendText("デモ4動作開始:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D517.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD517", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD517", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                    }
                    //D10518～D10520
                    {
                        //D10518,15
                        {
                            Label = "動作なし";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("動作なし:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D518] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D518] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D518] = Old_D[(int)DeviceID.D518];
                                    OSC.Sender(new OscMessage("/CLD518", BitData));
                                }
                                OSC_Log.AppendText("動作なし:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D518.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD518", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD518", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10519,16
                        {
                            Label = "動作正常終了";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("動作正常終了:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D519] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D519] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D519] = Old_D[(int)DeviceID.D519];
                                    OSC.Sender(new OscMessage("/CLD519", BitData));
                                }
                                OSC_Log.AppendText("動作正常終了:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D519.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD519", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD519", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                        //D10520,17
                        {
                            Label = "動作強制終了";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("動作強制終了:" + BitData.ToString() + "\r\n");
                            if (DebugSwitch.Checked == false)
                            {
                                Old_D[(int)DeviceID.D520] = BitData;
                                if (Old_D_Comparison[(int)DeviceID.D520] != BitData)
                                {
                                    Old_D_Comparison[(int)DeviceID.D520] = Old_D[(int)DeviceID.D520];
                                    OSC.Sender(new OscMessage("/CLD520", BitData));
                                }
                                OSC_Log.AppendText("動作強制終了:" + BitData.ToString() + "\r\n");
                            }
                            else
                            {
                                if (D520.Checked == false)
                                {
                                    OSC.Sender(new OscMessage("/CLD520", 0));
                                    OSC_Log.AppendText(Label + ":" + 0 + "\r\n");
                                }
                                else
                                {
                                    OSC.Sender(new OscMessage("/CLD520", 1));
                                    OSC_Log.AppendText(Label + ":" + 1 + "\r\n");
                                }
                            }
                        }
                    }
                }
                //PLCへ送信するデータ表示
                {
                    //項目説明
                    {
                        LogView.AppendText("\r\n" + "//パスボックスWrite" + "\r\n");
                    }
                    //D10001～D10005
                    {
                        //D10001
                        {
                            Label = "PCライフビット";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("PCライフビット:" + BitData.ToString() + "\r\n");
                        }
                        //D10002
                        {
                            Label = "ゲーム中";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("ゲーム中:" + BitData.ToString() + "\r\n");
                        }
                        //D10004
                        {
                            Label = "結果1の1";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("結果1-1:" + BitData.ToString() + "\r\n");
                        }
                        //D10005
                        {
                            Label = "結果1の2";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("結果1-2:" + BitData.ToString() + "\r\n");
                        }
                        //D10006
                        {
                            Label = "結果1の3";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("結果1-3:" + BitData.ToString() + "\r\n");
                        }
                        //D10007
                        {
                            Label = "結果2の1";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("結果2-1:" + BitData.ToString() + "\r\n");
                        }
                        //D10008
                        {
                            Label = "結果2の2";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("結果2-2:" + BitData.ToString() + "\r\n");
                        }
                        //D10009
                        {
                            Label = "結果2の3";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("結果2-3:" + BitData.ToString() + "\r\n");
                        }
                        //D10010
                        {
                            Label = "結果3の1";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("結果3-1:" + BitData.ToString() + "\r\n");
                        }
                        //D10011
                        {
                            Label = "結果3の2";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("結果3-2:" + BitData.ToString() + "\r\n");
                        }
                        //D10012
                        {
                            Label = "結果3の3";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("結果3-3:" + BitData.ToString() + "\r\n");
                        }
                        //D10013
                        {
                            Label = "結果4の1";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("結果4-1:" + BitData.ToString() + "\r\n");
                        }
                        //D10014
                        {
                            Label = "結果4の2";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("結果4-2:" + BitData.ToString() + "\r\n");
                        }
                        //D10015
                        {
                            Label = "結果4の3";
                            DataBox = dotUtlType.GetDevice(ref Label, ref BitData);
                            LogView.AppendText("結果4-3:" + BitData.ToString() + "\r\n");
                        }
                    }
                }
                //OSC_Log
                {
                    Packet_Log.Text = "//UnityからのパケットData" + "\r\n";
                    Packet_Log.AppendText(ReceiveData + "\r\n");
                    OSCGameStateCheck(ReceiveData);
                }
                //間隔待ち
                await Task.Delay((int)IntervalTime.TotalMilliseconds);
            }
        }
        //ビットデータを0に戻す
        private void BitRefresh_0(string LabelName)
        {
            Label = LabelName;
            BitData = 0;
            DataBox = dotUtlType.SetDevice(ref Label, BitData);
        }
        //PLCへの結果1-1:送信//
        private async void Result_11_Click(object sender, EventArgs e)
        {
            {
                Label = "結果1の1";
                BitData = 1;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            {
                await Task.Delay(3000);
                BitRefresh_0("結果1の1");
            }
        }
        //PLCへの結果1-2:送信//
        private async void Result_12_Click(object sender, EventArgs e)
        {
            {
                Label = "結果1の2";
                BitData = 1;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            {
                await Task.Delay(3000);
                BitRefresh_0("結果1の2");
            }
        }
        //PLCの結果1-3:送信//
        private async void Result_13_Click(object sender, EventArgs e)
        {
            {
                Label = "結果1の3";
                BitData = 1;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            {
                await Task.Delay(3000);
                BitRefresh_0("結果1の3");
            }
        }
        //PLCの結果2-1:送信//
        private async void Result_21_Click(object sender, EventArgs e)
        {
            {
                Label = "結果2の1";
                BitData = 1;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            {
                await Task.Delay(3000);
                BitRefresh_0("結果2の1");
            }
        }
        //PLCの結果2-2:送信//
        private async void Result_22_Click(object sender, EventArgs e)
        {
            {
                Label = "結果2の2";
                BitData = 1;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            {
                await Task.Delay(3000);
                BitRefresh_0("結果2の2");
            }
        }
        //PLCの結果2-3:送信//
        private async void Result_23_Click(object sender, EventArgs e)
        {
            {
                Label = "結果2の3";
                BitData = 1;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            {
                await Task.Delay(3000);
                BitRefresh_0("結果2の3");
            }
        }
        //PLCの結果3-1:送信//
        private async void Result_31_Click(object sender, EventArgs e)
        {
            {
                Label = "結果3の1";
                BitData = 1;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            {
                await Task.Delay(3000);
                BitRefresh_0("結果3の1");
            }
        }
        //PLCの結果3-2:送信//
        private async void Result_32_Click(object sender, EventArgs e)
        {
            {
                Label = "結果3の2";
                BitData = 1;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            {
                await Task.Delay(3000);
                BitRefresh_0("結果3の2");
            }
        }
        //PLCの結果3-3:送信//
        private async void Result_33_Click(object sender, EventArgs e)
        {
            {
                Label = "結果3の3";
                BitData = 1;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            {
                await Task.Delay(3000);
                BitRefresh_0("結果3の3");
            }
        }
        //PLCの結果4-1:送信//
        private async void Result_41_Click(object sender, EventArgs e)
        {
            {
                Label = "結果4の1";
                BitData = 1;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            {
                await Task.Delay(3000);
                BitRefresh_0("結果4の1");
            }
        }
        //PLCの結果4-2:送信//
        private async void Result_42_Click(object sender, EventArgs e)
        {
            {
                Label = "結果4の2";
                BitData = 1;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            {
                await Task.Delay(3000);
                BitRefresh_0("結果4の2");
            }
        }
        //PLCの結果4-3:送信//
        private async void Result_43_Click(object sender, EventArgs e)
        {
            {
                Label = "結果4の3";
                BitData = 1;
                DataBox = dotUtlType.SetDevice(ref Label, BitData);
            }
            {
                await Task.Delay(3000);
                BitRefresh_0("結果4の3");
            }
        }
        //チェックボックスのAllリセットボタン
        private void CheckBox_Reset_Click(object sender, EventArgs e)
        {
            D501.Checked = false;
            D502.Checked = false;
            D504.Checked = false;
            D505.Checked = false;
            D506.Checked = false;
            D507.Checked = false;
            D508.Checked = false;
            D509.Checked = false;
            D510.Checked = false;
            D512.Checked = false;
            D513.Checked = false;
            D514.Checked = false;
            D515.Checked = false;
            D516.Checked = false;
            D517.Checked = false;
            D518.Checked = false;
            D519.Checked = false;
            D520.Checked = false;
        }
    }
    //OSC通信定義・Unityとの通信用クラス
    class OSC
    {
        //受信先用
        private static OscReceiver oscReceiver;
        private static Task oscReceiverTask = null;

        //使用端末のIPの取得
        static string getIPAddress()
        {
            string ipaddress = "";
            IPHostEntry ipentry = Dns.GetHostEntry(Dns.GetHostName());

            foreach (IPAddress ip in ipentry.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    ipaddress = ip.ToString();
                    break;
                }
            }
            return ipaddress;
        }

        //Unityへの送信//
        public static void Sender(OscMessage OscM)
        {
            //送信先のIPアドレス設定
            IPAddress Address = IPAddress.Parse(getIPAddress());
            //送受信用ポート設定
            int port = Properties.Settings.Default.OSC_SenderPort;//36865
            //OscSenderPortの作成
            using (OscSender oscSender = new OscSender(Address, port))
            {
                //Dender接続
                oscSender.Connect();
                //データの送信
                oscSender.Send(OscM);
            }
        }

        //Unityからの受信//
        public static void Receiver()
        {
            //送受信用ポート設定
            int port = Properties.Settings.Default.OSC_ReceivePort;//21463
            //ReceiverPort作成
            oscReceiver = new OscReceiver(port);
            //Receiverへ接続
            oscReceiver.Connect();
            oscReceiverTask = new Task(() => OscListenProcess());
            oscReceiverTask.Start();
        }

        private static void OscListenProcess()
        {
            try
            {
                //Receiverの更新()
                while (oscReceiver.State != OscSocketState.Closed)
                {
                    //受信待ち
                    OscPacket oscPacket = oscReceiver.Receive();
                    //受信内容の確認
                    MainForm.ReceiveData = oscPacket.ToString();
                }
            }
            catch (Exception ex)
            {
                /* 例外処理　発生時はコンソールに出力
                 * ただし
                 * m_OscReceiver.Receive()で受信待ち状態の時に終了処理(m_OscReceiver.close())をすると
                 * 正しい処理でもExceptionnとなるため、接続中かで正しい処理か例外かを判断する
                 */
                if (oscReceiver.State == OscSocketState.Connected)
                {
                    //エラー表示
                    MainForm.ReceiveData = ex.Message.ToString();
                }
            }
        }

        public static void OscClosed()
        {
            //通信を切断
            oscReceiver.Close();
        }
    }
}